package com.tmarzeion.carbuddy;

import com.tmarzeion.carbuddy.activities.status.StatusContract;
import com.tmarzeion.carbuddy.activities.status.StatusPresenter;
import com.tmarzeion.carbuddy.handlers.NotificationHandler;
import com.tmarzeion.carbuddy.services.CarBuddyService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StatusPresenterTest {

    @Mock
    private StatusContract.View view;

    @Mock
    private NotificationHandler notificationHandler;

    @Mock
    private CarBuddyService carBuddyService;

    private StatusPresenter statusPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        statusPresenter = new StatusPresenter(view);
        statusPresenter.setCarBuddyService(carBuddyService);
    }

    @Test
    public void initialization_shouldAddWalletMarkerIfPosDefined() {
        when(carBuddyService.isWalletPositionDefined()).thenReturn(Boolean.TRUE);
        statusPresenter.start();
        verify(view).addWalletMarker(anyObject());
    }
    @Test
    public void initialization_shouldNotAddWalletMarkerIfPosNotDefined() {
        when(carBuddyService.isWalletPositionDefined()).thenReturn(Boolean.FALSE);
        statusPresenter.start();
        verify(view, never()).addWalletMarker(anyObject());
    }

    @Test
    public void initialization_shouldAddCarMarkerIfPosDefined() {
        when(carBuddyService.isCarPositionDefined()).thenReturn(Boolean.TRUE);
        statusPresenter.start();
        verify(view).addCarMarker(anyObject());
    }
    @Test
    public void initialization_shouldNotAddCarMarkerIfPosNotDefined() {
        when(carBuddyService.isCarPositionDefined()).thenReturn(Boolean.FALSE);
        statusPresenter.start();
        verify(view, never()).addCarMarker(anyObject());
    }

    @Test
    public void initialization_shouldShowSnackbarIfNotBothPositionsDefined() {
        when(carBuddyService.isCarPositionDefined()).thenReturn(Boolean.FALSE);
        when(carBuddyService.isWalletPositionDefined()).thenReturn(Boolean.TRUE);
        statusPresenter.start();
        verify(view).disableServiceSwitch();
        verify(view).showRequirementsSnackbar();
    }
    @Test
    public void initialization_shouldEnableServiceSwitchIfBothPositionsDefined() {
        when(carBuddyService.isCarPositionDefined()).thenReturn(Boolean.TRUE);
        when(carBuddyService.isWalletPositionDefined()).thenReturn(Boolean.TRUE);
        statusPresenter.start();
        verify(view).enableServiceSwitch();
    }

    @Test
    public void resumeApp_shouldDeactivateServiceSwitchIfServiceIsNotActivated() {
        when(carBuddyService.isActivated()).thenReturn(Boolean.FALSE);
        statusPresenter.viewResumed();
        verify(view).deactivateServiceSwitch();
    }

    @Test
    public void walletNotFound_shouldShowNotificationIfAppIsNotVisible() {
        when(view.isActive()).thenReturn(Boolean.FALSE);
        statusPresenter.onForgotWallet(notificationHandler);
        verify(notificationHandler).showForgotWalletNotification();
    }

    @Test
    public void walletNotFound_shouldShowAlertDialogIfAppIsVisible() {
        when(view.isActive()).thenReturn(Boolean.TRUE);
        statusPresenter.onForgotWallet(notificationHandler);
        verify(view).showForgotWalletAlertDialog();
    }
    
    

}