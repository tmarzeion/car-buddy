package com.tmarzeion.carbuddy.activities.status;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tmarzeion.carbuddy.R;
import com.tmarzeion.carbuddy.util.ApplicationConstants;

/**
* This Fragment is responsible for:
* - Displaying and handling events of Map and other View components
*/
public class StatusFragment extends Fragment implements StatusContract.View,
        OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {


    // Presenter
    private StatusContract.Presenter presenter;

    // Google Map
    private GoogleMap googleMap;

    // Markers
    private Marker carMarker;
    private Marker walletMarker;

    // Markers' circles
    private Circle carMarkerCircle;
    private Circle walletMarkerCircle;

    // Buttons Container and Switch
    private RelativeLayout interfaceComponentsContainer;

    // Service switch
    private FloatingActionButton serviceSwitchButton;
    private boolean serviceSwitchState;

    // Car menu buttons
    private FloatingActionMenu carMenuButton;
    private FloatingActionButton carLocationManualButton;
    private FloatingActionButton carLocationUseDeviceButton;
    private FloatingActionButton carLocationRemoveMarkerButton;

    // Wallet menu buttons
    private FloatingActionMenu walletMenuButton;
    private FloatingActionButton walletLocationManualButton;
    private FloatingActionButton walletLocationUseDeviceButton;
    private FloatingActionButton walletLocationRemoveMarkerButton;

    // Splash Screen
    private RelativeLayout splashScreen;

    // Initial Progress Bar
    private ProgressBar splashScreenProgressBar;

    // Manual position defining Tooltip Snackbar
    private Snackbar snackbarDefineManualPosition;

    // Both positions requirement Tooltip Snackbar
    private Snackbar snackbarCarAndWalletPosRequired;

    // Marker edit mode state
    private enum EditState {
        NONE, EDITING_CAR_POS, EDITING_WALLET_POS
    }
    private EditState editState = EditState.NONE;

    // Flag that tells if App is on background or foreground
    private boolean isActive;

    public static StatusFragment newInstance() {
        return new StatusFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_status, container, false);

        initializeViewFields(root);
        initializeListeners();
        initializeMap();

        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize Snackbars
        snackbarDefineManualPosition = Snackbar.make(view, R.string.snackbar_set_position_manually_tooltip, Snackbar.LENGTH_LONG);
        snackbarCarAndWalletPosRequired = Snackbar.make(view, R.string.snackbar_both_pos_required,
                Snackbar.LENGTH_INDEFINITE).setAction(R.string.snackbar_dismiss, v -> snackbarCarAndWalletPosRequired.dismiss());

        // Put position requirements snackbar on top of screen
        View snackbarView = snackbarCarAndWalletPosRequired.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackbarView.getLayoutParams();
        params.gravity = Gravity.TOP;
        snackbarView.setLayoutParams(params);
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.viewResumed();
        isActive = true;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setPresenter(StatusContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public void initializeMap() {
        // Attach Map Fragment to Activity
        FragmentManager fm = getChildFragmentManager();
        MapFragment mapFragment = (MapFragment) fm.findFragmentByTag("mapFragment");
        if (mapFragment == null) {
            mapFragment = new MapFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.mapFragmentContainer, mapFragment, "mapFragment");
            ft.commit();
            fm.executePendingTransactions();
        }
        this.splashScreenProgressBar.setVisibility(View.VISIBLE);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        configureMap();
        this.presenter.start();
    }

    private void configureMap() {
        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.setOnMapLoadedCallback(this);

        initializeOnMyLocationButtonClickListener();
        initializeOnMapClickListener();
        initializeMarkerDragListener();
    }

    @Override
    public void onMapLoaded() {
        this.presenter.mapLoaded();
    }

    @Override
    public void closeCarMenuButton(boolean animate) {
        carMenuButton.close(animate);
    }

    @Override
    public void closeWalletMenuButton(boolean animate) {
        walletMenuButton.close(animate);
    }

    @Override
    public void setRemoveCarMarkerButtonVisibility(boolean isVisible) {
        setFabVisibility(carLocationRemoveMarkerButton, isVisible);
    }

    @Override
    public void setRemoveWalletMarkerButtonVisibility(boolean isVisible) {
        setFabVisibility(walletLocationRemoveMarkerButton, isVisible);
    }

    private void setFabVisibility(FloatingActionButton fab, boolean isVisible) {
        if (isVisible) {
            fab.setClickable(true);
            fab.setVisibility(View.VISIBLE);
            fab.setLabelVisibility(View.VISIBLE);
        } else {
            fab.setClickable(false);
            fab.hide(true);
            fab.setLabelVisibility(View.GONE);
        }
    }

    @Override
    public void addCarMarker(LatLng carPosition) {
        carMarker = addMarker(carPosition, ApplicationConstants.MarkerTags.CAR_MARKER_TAG, BitmapDescriptorFactory.HUE_ORANGE);
        carMarkerCircle = addCircle(carPosition, ApplicationConstants.Distances.CAR_BEACON_RADIUS, R.color.car_circle_fill_color);
    }

    @Override
    public void addWalletMarker(LatLng walletPosition) {
        walletMarker = addMarker(walletPosition, ApplicationConstants.MarkerTags.WALLET_MARKER_TAG, 70f);
        walletMarkerCircle = addCircle(walletPosition, ApplicationConstants.Distances.WALLET_BEACON_RADIUS, R.color.wallet_circle_fill_color);
    }

    private Marker addMarker(LatLng markerPosition, String markerTag, float markerColorHue) {
        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(markerPosition));
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(markerColorHue));
        marker.setTag(markerTag);
        marker.setDraggable(true);
        return marker;
    }

    private Circle addCircle(LatLng circlePosition, double radius, int colorId) {
        CircleOptions circleOptions = new CircleOptions()
                .center(circlePosition)
                .radius(radius)
                .fillColor(ContextCompat.getColor(this.getActivity().getBaseContext(), colorId))
                .strokeWidth(0);
        return googleMap.addCircle(circleOptions);
    }

    @Override
    public void removeCarMarker() {
        carMarker.remove();
        carMarkerCircle.remove();
        carMarker = null;
        carMarkerCircle = null;
    }

    @Override
    public void removeWalletMarker() {
        walletMarker.remove();
        walletMarkerCircle.remove();
        walletMarker = null;
        walletMarkerCircle = null;
    }

    @Override
    public void setCarMarkerPosition(LatLng position) {
        if (carMarker == null) {
            addCarMarker(position);
            setRemoveCarMarkerButtonVisibility(true);
        }
        carMarker.setPosition(position);
        carMarkerCircle.setCenter(position);
    }

    @Override
    public void setWalletMarkerPosition(LatLng position) {
        if (walletMarker == null) {
            addWalletMarker(position);
            setRemoveWalletMarkerButtonVisibility(true);
        }
        walletMarker.setPosition(position);
        walletMarkerCircle.setCenter(position);
    }

    @Override
    public void zoomToShowAllMarkers(LatLng devicePosition) {
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        int boundPoints = 0;
        if (carMarker != null) {
            boundsBuilder.include(carMarker.getPosition());
            boundPoints++;
        }
        if (walletMarker != null) {
            boundsBuilder.include(walletMarker.getPosition());
            boundPoints++;
        }
        if (devicePosition != null) {
            boundsBuilder.include(devicePosition);
            boundPoints++;
        }

        if (boundPoints > 1) {
            int padding = ApplicationConstants.Dimensions.ZOOM_MARKER_BORDER_OFFSET;
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), padding));
        } else {
            if (devicePosition != null) {
                zoomToPosition(devicePosition);
            }
        }
    }

    private void zoomToPosition(LatLng position) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position,
                ApplicationConstants.Dimensions.ZOOM_LVL_FOR_ONE_POSITION));
    }

    private void zoomToShowCarMarker() {
        zoomToPosition(carMarker.getPosition());
    }

    private void zoomToShowWalletMarker() {
        zoomToPosition(walletMarker.getPosition());
    }

    @Override
    public boolean toggleServiceSwitchState() {
        serviceSwitchState = !serviceSwitchState;
        return serviceSwitchState;
    }

    @Override
    public void deactivateServiceSwitch() {
        serviceSwitchState = false;
        if (serviceSwitchButton.isEnabled()) {
            serviceSwitchButton.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_notifications_off));
        } else {
            serviceSwitchButton.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_notifications_off_disabled));
        }
        serviceSwitchButton.setColorNormal(ContextCompat.getColor(this.getActivity(), R.color.switch_button_deactivated_normal));
        serviceSwitchButton.setColorPressed(ContextCompat.getColor(this.getActivity(), R.color.switch_button_deactivated_pressed));
    }

    @Override
    public void activateServiceSwitch() {
        serviceSwitchState = true;
        serviceSwitchButton.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_notifications_on));
        serviceSwitchButton.setColorNormal(ContextCompat.getColor(this.getActivity(), R.color.switch_button_activated_normal));
        serviceSwitchButton.setColorPressed(ContextCompat.getColor(this.getActivity(), R.color.switch_button_activated_pressed));
    }

    @Override
    public void enableServiceSwitch() {
        if (serviceSwitchState) {
            serviceSwitchButton.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_notifications_on));
        } else {
            serviceSwitchButton.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_notifications_off));
        }
        serviceSwitchButton.setEnabled(true);
    }

    @Override
    public void disableServiceSwitch() {
        serviceSwitchButton.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_notifications_off_disabled));
        serviceSwitchButton.setEnabled(false);
    }

    @Override
    public void showForgotWalletAlertDialog() {
        new AlertDialog.Builder(this.getActivity())
                .setTitle(getResources().getString(R.string.alert_wallet_not_found))
                .setMessage(getResources().getString(R.string.alert_dont_forget_wallet))
                .setPositiveButton(getResources().getString(R.string.alert_ok), null)
                .show();
    }

    @Override
    public void showRequirementsSnackbar() {
        snackbarCarAndWalletPosRequired.getView().setVisibility(View.VISIBLE);
        snackbarCarAndWalletPosRequired.show();
    }

    @Override
    public void dismissRequirementsSnackbar() {
        snackbarCarAndWalletPosRequired.getView().setVisibility(View.GONE);
        snackbarCarAndWalletPosRequired.dismiss();
    }

    @Override
    public void hideSplashScreen() {
        this.interfaceComponentsContainer.setVisibility(View.VISIBLE);
        this.splashScreen.setVisibility(View.GONE);
        this.splashScreenProgressBar.setVisibility(View.GONE);
    }

    private void setUIState(EditState state) {
        this.editState = state;
        switch (editState) {
            case NONE:
                showUIComponents();
                snackbarDefineManualPosition.dismiss();
                break;
            case EDITING_CAR_POS:
                dismissRequirementsSnackbar();
                hideUIComponents();
                snackbarDefineManualPosition.setText(String.format(getString(R.string.snackbar_set_position_manually_tooltip), getString(R.string.car)));
                snackbarDefineManualPosition.show();
                break;
            case EDITING_WALLET_POS:
                dismissRequirementsSnackbar();
                hideUIComponents();
                snackbarDefineManualPosition.setText(String.format(getString(R.string.snackbar_set_position_manually_tooltip), getString(R.string.wallet)));
                snackbarDefineManualPosition.show();
                break;
        }
    }

    private void hideUIComponents() {
        walletMenuButton.hideMenuButton(true);
        carMenuButton.hideMenuButton(true);
        serviceSwitchButton.setVisibility(View.GONE);
    }

    private void showUIComponents() {
        walletMenuButton.showMenuButton(true);
        carMenuButton.showMenuButton(true);
        serviceSwitchButton.setVisibility(View.VISIBLE);
    }

    private void initializeViewFields(View root) {
        // Initialize splash screen components
        splashScreen = ((RelativeLayout) getActivity().findViewById(R.id.splashScreen));
        splashScreenProgressBar = ((ProgressBar) getActivity().findViewById(R.id.splashScreenProgressBar));

        // Initialize button container
        interfaceComponentsContainer = (RelativeLayout) root.findViewById(R.id.buttonContainer);

        // Initialize service switch
        serviceSwitchButton = (FloatingActionButton) root.findViewById(R.id.serviceSwitchButton);

        // Initialize Car menu buttons
        carMenuButton = ((FloatingActionMenu) root.findViewById(R.id.car_location_menu));
        carLocationManualButton = ((FloatingActionButton) root.findViewById(R.id.car_location_button_type_manually));
        carLocationUseDeviceButton = ((FloatingActionButton) root.findViewById(R.id.car_location_button_use_device));
        carLocationRemoveMarkerButton = ((FloatingActionButton) root.findViewById(R.id.car_location_button_remove_marker));

        // Initialize Wallet menu buttons
        walletMenuButton = ((FloatingActionMenu) root.findViewById(R.id.wallet_location_menu));
        walletLocationManualButton = ((FloatingActionButton) root.findViewById(R.id.wallet_location_button_type_manually));
        walletLocationUseDeviceButton = ((FloatingActionButton) root.findViewById(R.id.wallet_location_button_use_device));
        walletLocationRemoveMarkerButton = ((FloatingActionButton) root.findViewById(R.id.wallet_location_button_remove_marker));
    }

    private void initializeListeners() {
        walletMenuButton.setOnMenuButtonClickListener(v -> {
            if (walletMarker == null) {
                walletLocationRemoveMarkerButton.setVisibility(View.GONE);
            }
            carMenuButton.close(true);
            walletMenuButton.toggle(true);
        });

        carMenuButton.setOnMenuButtonClickListener(v -> {
            if (carMarker == null) {
                carLocationRemoveMarkerButton.setVisibility(View.GONE);
            }
            walletMenuButton.close(true);
            carMenuButton.toggle(true);
        });

        walletMenuButton.setOnMenuButtonLongClickListener(v -> {
            zoomToShowWalletMarker();
            return true;
        });

        carMenuButton.setOnMenuButtonLongClickListener(v -> {
            zoomToShowCarMarker();
            return true;
        });

        walletLocationUseDeviceButton.setOnClickListener(v -> presenter.setWalletPositionAsCurrentDevicePositionButtonClicked());
        carLocationUseDeviceButton.setOnClickListener(v -> presenter.setCarPositionAsCurrentDevicePositionButtonClicked());

        walletLocationManualButton.setOnClickListener(v -> setUIState(EditState.EDITING_WALLET_POS));
        carLocationManualButton.setOnClickListener(v -> setUIState(EditState.EDITING_CAR_POS));

        walletLocationRemoveMarkerButton.setOnClickListener(v -> presenter.walletMarkerRemoveButtonClicked());
        carLocationRemoveMarkerButton.setOnClickListener(v -> presenter.carMarkerRemoveButtonClicked());

        serviceSwitchButton.setOnClickListener(v -> presenter.serviceSwitchClicked());
    }

    private void initializeOnMyLocationButtonClickListener() {
        this.googleMap.setOnMyLocationButtonClickListener(() -> {
            presenter.clickedMyLocationButton();
            return true;
        });
    }

    private void initializeOnMapClickListener() {
        this.googleMap.setOnMapClickListener(latLng -> {
            switch (editState) {
                case NONE:
                    carMenuButton.close(true);
                    walletMenuButton.close(true);
                    break;
                case EDITING_CAR_POS:
                    setCarMarkerPosition(latLng);
                    presenter.carPosChanged(latLng);
                    setUIState(EditState.NONE);
                    showUIComponents();
                    break;
                case EDITING_WALLET_POS:
                    setWalletMarkerPosition(latLng);
                    presenter.walletPosChanged(latLng);
                    setUIState(EditState.NONE);
                    showUIComponents();
                    break;
                default:
                    // Do nothing
                    break;
            }
        });
    }

    private void initializeMarkerDragListener() {
        this.googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                if (carMarker != null && marker.getTag().equals(carMarker.getTag())) {
                    carMarkerCircle.setVisible(false);
                } else if (walletMarker != null && marker.getTag().equals(walletMarker.getTag())) {
                    walletMarkerCircle.setVisible(false);
                }
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                // Do nothing
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                LatLng markerPosition = marker.getPosition();
                if (carMarker != null && marker.getTag().equals(carMarker.getTag())) {
                    carMarkerCircle.setCenter(markerPosition);
                    carMarkerCircle.setVisible(true);
                    presenter.carPosChanged(markerPosition);
                } else if (walletMarker != null && marker.getTag().equals(walletMarker.getTag())) {
                    walletMarkerCircle.setCenter(markerPosition);
                    walletMarkerCircle.setVisible(true);
                    presenter.walletPosChanged(marker.getPosition());
                }
            }
        });
    }

}
