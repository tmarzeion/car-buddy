package com.tmarzeion.carbuddy.activities.status;

import com.google.android.gms.maps.model.LatLng;
import com.tmarzeion.carbuddy.handlers.NotificationHandler;
import com.tmarzeion.carbuddy.listeners.CarBuddyServiceListener;
import com.tmarzeion.carbuddy.services.CarBuddyService;

/**
* This Presenter is responsible for:
* - Manipulating View
* - Handling UI events from View
* - Communicating with core application service
*/
public class StatusPresenter implements StatusContract.Presenter, CarBuddyServiceListener {

    // View
    private StatusContract.View statusView;

    // Car Buddy Service
    private CarBuddyService carBuddyService;

    public StatusPresenter(StatusContract.View view) {
        this.statusView = view;
        this.statusView.setPresenter(this);
    }

    public void setCarBuddyService(CarBuddyService service) {
        this.carBuddyService = service;
        this.carBuddyService.setEventListener(this);
    }

    /**
     * Events coming from Fragment:
     */

    @Override
    public void start() {
        statusView.hideSplashScreen();

        boolean isWalletPositionDefined = carBuddyService.isWalletPositionDefined();
        boolean isCarPositionDefined = carBuddyService.isCarPositionDefined();

        if (isWalletPositionDefined) {
            statusView.addWalletMarker(carBuddyService.getWalletPosition());
            statusView.setRemoveWalletMarkerButtonVisibility(true);
        }
        if (isCarPositionDefined) {
            statusView.addCarMarker(carBuddyService.getCarPosition());
            statusView.setRemoveCarMarkerButtonVisibility(true);
        }

        if (!isCarPositionDefined || !isWalletPositionDefined) {
            statusView.disableServiceSwitch();
            statusView.showRequirementsSnackbar();
        } else {
            statusView.enableServiceSwitch();
        }
    }

    @Override
    public void carPosChanged(LatLng newPosition) {
        carBuddyService.carPosChanged(newPosition);
        statusView.zoomToShowAllMarkers(carBuddyService.getDeviceLocation());
    }

    @Override
    public void walletPosChanged(LatLng newPosition) {
        carBuddyService.walletPosChanged(newPosition);
        statusView.zoomToShowAllMarkers(carBuddyService.getDeviceLocation());
    }

    @Override
    public void setCarPositionAsCurrentDevicePositionButtonClicked() {
        LatLng devicePosition = carBuddyService.getDeviceLocation();
        statusView.setCarMarkerPosition(devicePosition);
        carPosChanged(devicePosition);
        statusView.closeCarMenuButton(true);
    }

    @Override
    public void setWalletPositionAsCurrentDevicePositionButtonClicked() {
        LatLng devicePosition = carBuddyService.getDeviceLocation();
        statusView.setWalletMarkerPosition(devicePosition);
        walletPosChanged(devicePosition);
        statusView.closeWalletMenuButton(true);
    }

    @Override
    public void carMarkerRemoveButtonClicked() {
        statusView.setRemoveCarMarkerButtonVisibility(false);
        statusView.removeCarMarker();
        onRequiredPositionsNotDefined();
        carBuddyService.carMarkerRemoved();
        statusView.zoomToShowAllMarkers(carBuddyService.getDeviceLocation());
    }

    @Override
    public void walletMarkerRemoveButtonClicked() {
        statusView.setRemoveWalletMarkerButtonVisibility(false);
        statusView.removeWalletMarker();
        onRequiredPositionsNotDefined();
        carBuddyService.walletMarkerRemoved();
        statusView.zoomToShowAllMarkers(carBuddyService.getDeviceLocation());
    }

    @Override
    public void clickedMyLocationButton() {
        statusView.zoomToShowAllMarkers(carBuddyService.getDeviceLocation());
    }

    @Override
    public void viewResumed() {
        if (!carBuddyService.isActivated()) {
            statusView.deactivateServiceSwitch();
        }
    }

    @Override
    public void mapLoaded() {
        statusView.zoomToShowAllMarkers(carBuddyService.getDeviceLocation());
    }

    @Override
    public void serviceSwitchClicked() {
        boolean isActivated = statusView.toggleServiceSwitchState();
        if (isActivated) {
            statusView.activateServiceSwitch();
            carBuddyService.activate();
        } else {
            statusView.deactivateServiceSwitch();
            carBuddyService.deactivate();
        }
    }

    /**
     * Events coming from CarBuddyService:
     */

    @Override
    public void onForgotWallet(NotificationHandler notificationHandler) {
        if (statusView.isActive()) {
            statusView.showForgotWalletAlertDialog();
            statusView.deactivateServiceSwitch();
        } else {
            notificationHandler.showForgotWalletNotification();
        }
    }

    @Override
    public void onDeactivated() {
        statusView.deactivateServiceSwitch();
    }

    @Override
    public void onRequiredPositionsDefined() {
        statusView.dismissRequirementsSnackbar();
        statusView.enableServiceSwitch();
    }

    @Override
    public void onRequiredPositionsNotDefined() {
        statusView.showRequirementsSnackbar();
        statusView.deactivateServiceSwitch();
        statusView.disableServiceSwitch();
    }

}
