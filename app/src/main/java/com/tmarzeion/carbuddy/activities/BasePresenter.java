package com.tmarzeion.carbuddy.activities;

public interface BasePresenter {

    void start();

}
