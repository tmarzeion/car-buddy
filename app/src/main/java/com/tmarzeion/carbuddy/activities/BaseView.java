package com.tmarzeion.carbuddy.activities;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
