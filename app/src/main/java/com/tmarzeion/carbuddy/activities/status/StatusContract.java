package com.tmarzeion.carbuddy.activities.status;

import com.google.android.gms.maps.model.LatLng;
import com.tmarzeion.carbuddy.activities.BasePresenter;
import com.tmarzeion.carbuddy.activities.BaseView;

public interface StatusContract {

    interface View extends BaseView<Presenter> {

        boolean isActive();

        void setRemoveCarMarkerButtonVisibility(boolean isVisible);

        void setRemoveWalletMarkerButtonVisibility(boolean isVisible);

        void addCarMarker(LatLng carPosition);

        void addWalletMarker(LatLng walletPosition);

        void zoomToShowAllMarkers(LatLng devicePosition);

        void setCarMarkerPosition(LatLng position);

        void setWalletMarkerPosition(LatLng position);

        void removeCarMarker();

        void removeWalletMarker();

        void activateServiceSwitch();

        void deactivateServiceSwitch();

        void disableServiceSwitch();

        void enableServiceSwitch();

        void showRequirementsSnackbar();

        void dismissRequirementsSnackbar();

        void showForgotWalletAlertDialog();

        void closeWalletMenuButton(boolean animate);

        void hideSplashScreen();

        void closeCarMenuButton(boolean animate);

        boolean toggleServiceSwitchState();
    }

    interface Presenter extends BasePresenter {

        void carPosChanged(LatLng newPosition);

        void walletPosChanged(LatLng newPosition);

        void setCarPositionAsCurrentDevicePositionButtonClicked();

        void setWalletPositionAsCurrentDevicePositionButtonClicked();

        void carMarkerRemoveButtonClicked();

        void walletMarkerRemoveButtonClicked();

        void serviceSwitchClicked();

        void viewResumed();

        void clickedMyLocationButton();

        void mapLoaded();

    }

}
