package com.tmarzeion.carbuddy.activities.status;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.tmarzeion.carbuddy.R;
import com.tmarzeion.carbuddy.handlers.GoogleApiHandler;
import com.tmarzeion.carbuddy.services.implementations.CarBuddyServiceImpl;
import com.tmarzeion.carbuddy.util.ActivityUtils;
import com.tmarzeion.carbuddy.util.ApplicationConstants;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
* This Activity is responsible for:
* - Creation of Presenter and its's View
* - Handling permissions (UI Included)
*/
@RuntimePermissions
public class StatusActivity extends Activity {

    // On permission denied components
    private LinearLayout onPermissionDeniedMessageButtonContainer;

    // On permission denied forever components
    private LinearLayout onNeverAskMessageAndButtonContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        // Initialize permissions components
        onPermissionDeniedMessageButtonContainer = ((LinearLayout) findViewById(R.id.onPermissionDeniedMessageButtonContainer));
        onNeverAskMessageAndButtonContainer = ((LinearLayout) findViewById(R.id.onNeverAskMessageAndButtonContainer));

        Button askForPermissionsAgainButton = ((Button) findViewById(R.id.askForPermissionsAgainButton));
        askForPermissionsAgainButton.setOnClickListener(v ->
                StatusActivityPermissionsDispatcher.initializeWithCheck(StatusActivity.this)
        );

        Button goToAppPermissionsButton = ((Button) findViewById(R.id.goToAppPermissionsButton));
        goToAppPermissionsButton.setOnClickListener(v -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", getPackageName(), null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

        StatusActivityPermissionsDispatcher.initializeWithCheck(StatusActivity.this);
    }

    @NeedsPermission({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    public void initialize() {
        SharedPreferences sharedPrefs = this.getSharedPreferences
                (ApplicationConstants.LocationPreferences.LOCATION_PREFERENCES_KEY, Context.MODE_PRIVATE);

        // Create Fragment
        StatusFragment statusFragment =
                (StatusFragment) getFragmentManager().findFragmentById(R.id.statusContentFrame);
        if (statusFragment == null) {
            statusFragment = StatusFragment.newInstance();
        }
        if (!statusFragment.isAdded()) {
            ActivityUtils.addFragmentToActivity(getFragmentManager(),
                    statusFragment, R.id.statusContentFrame);
        }

        // Create Presenter
        StatusPresenter presenter = new StatusPresenter(statusFragment);
        CarBuddyServiceImpl carBuddyService = new CarBuddyServiceImpl(sharedPrefs, this.getApplicationContext());
        presenter.setCarBuddyService(carBuddyService);
        new GoogleApiHandler(this, carBuddyService);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        StatusActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    // Show dialog that asks for permissions
    @OnShowRationale({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    protected void showRationaleForLocationPermissions(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_allow_location_label)
                .setPositiveButton(R.string.permission_allow_location_allow, (dialog, which) -> request.proceed())
                .setNegativeButton(R.string.permission_allow_location_deny, (dialog, button) -> request.cancel())
                .show();
    }

    // Show half-transparent overlay which suggests to enable location via button
    @OnPermissionDenied({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    protected void showDeniedForLocationPermissions() {
        onNeverAskMessageAndButtonContainer.setVisibility(View.GONE);
        onPermissionDeniedMessageButtonContainer.setVisibility(View.VISIBLE);
    }

    // Show half-transparent overlay which suggests to enable location manually
    @OnNeverAskAgain({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    protected void showNeverAskForLocationPermissions() {
        onNeverAskMessageAndButtonContainer.setVisibility(View.VISIBLE);
        onPermissionDeniedMessageButtonContainer.setVisibility(View.GONE);
    }

}
