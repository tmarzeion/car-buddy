package com.tmarzeion.carbuddy.util;

import com.google.android.gms.maps.model.LatLng;

public class DistanceUtils {

    public static double distanceBetweenTwoPositions(LatLng pos1, LatLng pos2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(pos2.latitude - pos1.latitude);
        double dLng = Math.toRadians(pos2.longitude - pos1.longitude);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(pos1.latitude)) * Math.cos(Math.toRadians(pos2.latitude)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (earthRadius * c);
    }

}
