package com.tmarzeion.carbuddy.util;

public class ApplicationConstants {

    public abstract class Distances {
        public static final double CAR_BEACON_RADIUS = 2; // METERS
        public static final double WALLET_BEACON_RADIUS = 2; // METERS
    }

    public abstract class MarkerTags {

        public static final String CAR_MARKER_TAG = "CAR_MARKER";
        public static final String WALLET_MARKER_TAG = "WALLET_MARKER";

    }

    public abstract class LocationPreferences {

        public static final String LOCATION_PREFERENCES_KEY = "LOCATION_PREFS";
        public static final String DEFAULT_LOCATION_LAT_LNG = "0";

        public abstract class CarKeys {
            public static final String POS_DEFINED = "CAR_POS_DEFINED";
            public static final String LAT_KEY = "CAR_LAT";
            public static final String LNG_KEY = "CAR_LNG";
        }

        public abstract class WalletKeys {
            public static final String POS_DEFINED = "WALLET_POS_DEFINED";
            public static final String LAT_KEY = "WALLET_LAT";
            public static final String LNG_KEY = "WALLET_LNG";
        }

        public abstract class DeviceKeys {
            public static final String LAT_KEY = "DEVICE_LAT";
            public static final String LNG_KEY = "DEVICE_LNG";
        }

    }

    public abstract class Dimensions {
        public static final int ZOOM_MARKER_BORDER_OFFSET = 50;
        public static final int ZOOM_LVL_FOR_ONE_POSITION = 20;
    }

}
