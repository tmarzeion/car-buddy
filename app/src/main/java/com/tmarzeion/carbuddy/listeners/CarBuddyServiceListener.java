package com.tmarzeion.carbuddy.listeners;

import com.tmarzeion.carbuddy.handlers.NotificationHandler;

public interface CarBuddyServiceListener {

    void onForgotWallet(NotificationHandler notificationHandler);

    void onDeactivated();

    void onRequiredPositionsDefined();

    void onRequiredPositionsNotDefined();

}
