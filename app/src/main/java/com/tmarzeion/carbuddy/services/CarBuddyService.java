package com.tmarzeion.carbuddy.services;

import com.google.android.gms.maps.model.LatLng;
import com.tmarzeion.carbuddy.listeners.CarBuddyServiceListener;

public interface CarBuddyService {

    void setEventListener(CarBuddyServiceListener listener);

    void activate();

    void deactivate();

    void walletPosChanged(LatLng newPosition);

    void carPosChanged(LatLng newPosition);

    LatLng getDeviceLocation();

    LatLng getWalletPosition();

    LatLng getCarPosition();

    boolean isWalletPositionDefined();

    boolean isCarPositionDefined();

    boolean isActivated();

    void walletMarkerRemoved();

    void carMarkerRemoved();
}
