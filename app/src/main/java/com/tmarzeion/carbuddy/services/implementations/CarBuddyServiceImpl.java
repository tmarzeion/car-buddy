package com.tmarzeion.carbuddy.services.implementations;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.LatLng;
import com.tmarzeion.carbuddy.handlers.NotificationHandler;
import com.tmarzeion.carbuddy.listeners.CarBuddyServiceListener;
import com.tmarzeion.carbuddy.services.CarBuddyService;
import com.tmarzeion.carbuddy.util.ApplicationConstants;
import com.tmarzeion.carbuddy.util.DistanceUtils;

/** This is core application service responsible for:
* - Updating location cache (shared prefs)
* - Handling all location change events (e.g. checking if user forgot wallet)
* - Communicating with event receivers
*/
public class CarBuddyServiceImpl implements CarBuddyService, LocationListener {

    private CarBuddyServiceListener eventReceiver;

    private SharedPreferences sharedPref;

    private LatLng carPos;
    private LatLng walletPos;
    private LatLng devicePos;

    private boolean isActivated;

    private boolean isWalletPositionDefined;
    private boolean isCarPositionDefined;

    private NotificationHandler notificationHandler;

    public CarBuddyServiceImpl(SharedPreferences sharedPref, Context context) {
        this.sharedPref = sharedPref;

        this.isCarPositionDefined = sharedPref.getBoolean(ApplicationConstants.LocationPreferences.CarKeys.POS_DEFINED, false);
        this.isWalletPositionDefined = sharedPref.getBoolean(ApplicationConstants.LocationPreferences.WalletKeys.POS_DEFINED, false);

        this.notificationHandler = new NotificationHandler(context);

        loadLastKnownPositions();
    }

    @Override
    public void onLocationChanged(Location location) {
        updatePositionPreferences(new LatLng(location.getLatitude(), location.getLongitude()),
                ApplicationConstants.LocationPreferences.DeviceKeys.LAT_KEY,
                ApplicationConstants.LocationPreferences.DeviceKeys.LNG_KEY);
        this.devicePos = new LatLng(location.getLatitude(), location.getLongitude());
        notifyListenerIfForgotWallet();
    }

    @Override
    public void setEventListener(CarBuddyServiceListener listener) {
        this.eventReceiver = listener;
    }

    @Override
    public void activate() {
        this.isActivated = true;
        notifyListenerIfForgotWallet();
    }

    @Override
    public void deactivate() {
        this.isActivated = false;
    }

    @Override
    public void carPosChanged(LatLng newPosition) {
        if (!isCarPositionDefined || !isWalletPositionDefined) {
            if (!isCarPositionDefined) {
                isCarPositionDefined = true;
                updateDefinedPositionsPreferences(ApplicationConstants.LocationPreferences.CarKeys.POS_DEFINED, true);
                if (isWalletPositionDefined && isCarPositionDefined) {
                    eventReceiver.onRequiredPositionsDefined();
                }
            }
            if (!isWalletPositionDefined) {
                eventReceiver.onRequiredPositionsNotDefined();
            }
        }

        updatePositionPreferences(newPosition,
                ApplicationConstants.LocationPreferences.CarKeys.LAT_KEY,
                ApplicationConstants.LocationPreferences.CarKeys.LNG_KEY);
        this.carPos = newPosition;

        deactivateIfWouldTriggerNotification();
    }

    @Override
    public void walletPosChanged(LatLng newPosition) {
        if (!isWalletPositionDefined || !isCarPositionDefined) {
            if (!isWalletPositionDefined) {
                isWalletPositionDefined = true;
                updateDefinedPositionsPreferences(ApplicationConstants.LocationPreferences.WalletKeys.POS_DEFINED, true);
                if (isWalletPositionDefined && isCarPositionDefined) {
                    eventReceiver.onRequiredPositionsDefined();
                }
            }
            if (!isCarPositionDefined) {
                eventReceiver.onRequiredPositionsNotDefined();
            }
        }
        updatePositionPreferences(newPosition,
                ApplicationConstants.LocationPreferences.WalletKeys.LAT_KEY,
                ApplicationConstants.LocationPreferences.WalletKeys.LNG_KEY);
        this.walletPos = newPosition;

        deactivateIfWouldTriggerNotification();
    }

    @Override
    public boolean isCarPositionDefined() {
        return isCarPositionDefined;
    }

    @Override
    public boolean isWalletPositionDefined() {
        return isWalletPositionDefined;
    }

    @Override
    public void carMarkerRemoved() {
        deactivate();
        carPos = null;
        isCarPositionDefined = false;
        updateDefinedPositionsPreferences(ApplicationConstants.LocationPreferences.CarKeys.POS_DEFINED, false);
    }

    @Override
    public void walletMarkerRemoved() {
        deactivate();
        walletPos = null;
        isWalletPositionDefined = false;
        updateDefinedPositionsPreferences(ApplicationConstants.LocationPreferences.WalletKeys.POS_DEFINED, false);
    }

    @Override
    public LatLng getDeviceLocation() {
        return devicePos;
    }

    @Override
    public LatLng getCarPosition() {
        return carPos;
    }

    @Override
    public LatLng getWalletPosition() {
        return walletPos;
    }

    @Override
    public boolean isActivated() {
        return isActivated;
    }

    private void deactivateIfWouldTriggerNotification() {
        if (isActivated) {
            if (checkIfForgotWallet()) {
                deactivate();
                eventReceiver.onDeactivated();
            }
        }
    }

    private void updatePositionPreferences(LatLng position, String latKey, String lngKey) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(latKey, String.valueOf(position.latitude));
        editor.putString(lngKey, String.valueOf(position.longitude));
        editor.apply();
    }

    private void updateDefinedPositionsPreferences(String key, boolean isDefined) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, isDefined);
        editor.apply();
    }

    private void loadLastKnownPositions() {
        carPos = new LatLng(
                Double.valueOf(sharedPref.getString(
                        ApplicationConstants.LocationPreferences.CarKeys.LAT_KEY,
                        ApplicationConstants.LocationPreferences.DEFAULT_LOCATION_LAT_LNG
                )),
                Double.valueOf(sharedPref.getString(
                        ApplicationConstants.LocationPreferences.CarKeys.LNG_KEY,
                        ApplicationConstants.LocationPreferences.DEFAULT_LOCATION_LAT_LNG
                )));

        walletPos = new LatLng(
                Double.valueOf(sharedPref.getString(
                        ApplicationConstants.LocationPreferences.WalletKeys.LAT_KEY,
                        ApplicationConstants.LocationPreferences.DEFAULT_LOCATION_LAT_LNG
                )),
                Double.valueOf(sharedPref.getString(
                        ApplicationConstants.LocationPreferences.WalletKeys.LNG_KEY,
                        ApplicationConstants.LocationPreferences.DEFAULT_LOCATION_LAT_LNG
                )));

        devicePos = new LatLng(
                Double.valueOf(sharedPref.getString(
                        ApplicationConstants.LocationPreferences.DeviceKeys.LAT_KEY,
                        ApplicationConstants.LocationPreferences.DEFAULT_LOCATION_LAT_LNG
                )),
                Double.valueOf(sharedPref.getString(
                        ApplicationConstants.LocationPreferences.DeviceKeys.LNG_KEY,
                        ApplicationConstants.LocationPreferences.DEFAULT_LOCATION_LAT_LNG
                )));
    }

    private boolean checkIfForgotWallet() {
        return checkIfForgotWallet(devicePos, carPos, walletPos);
    }

    private boolean checkIfForgotWallet(LatLng devicePos, LatLng carPos, LatLng walletPos) {
        boolean isDeviceInCarRadius =
                DistanceUtils.distanceBetweenTwoPositions(carPos, devicePos)
                        <= ApplicationConstants.Distances.CAR_BEACON_RADIUS;

        boolean isDeviceInWalletRadius =
                DistanceUtils.distanceBetweenTwoPositions(walletPos, devicePos)
                        <= ApplicationConstants.Distances.WALLET_BEACON_RADIUS;

        return isDeviceInCarRadius && !isDeviceInWalletRadius;
    }

    private void notifyListenerIfForgotWallet() {
        if (isActivated) {
            if (checkIfForgotWallet()) {
                eventReceiver.onForgotWallet(notificationHandler);
                deactivate();
            }
        }
    }

}
