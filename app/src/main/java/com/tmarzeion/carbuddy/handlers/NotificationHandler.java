package com.tmarzeion.carbuddy.handlers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.tmarzeion.carbuddy.R;
import com.tmarzeion.carbuddy.activities.status.StatusActivity;

public class NotificationHandler {

    private Context context;

    public NotificationHandler(Context context) {
        this.context = context;
    }

    public void showForgotWalletNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_car)
                        .setAutoCancel(true)
                        .setContentTitle(context.getResources().getString((R.string.alert_forgot_wallet_message)))
                        .setContentText(context.getResources().getString((R.string.alert_click_to_open_car_buddy)));

        Intent resultIntent = new Intent(context, StatusActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(StatusActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        builder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

}
