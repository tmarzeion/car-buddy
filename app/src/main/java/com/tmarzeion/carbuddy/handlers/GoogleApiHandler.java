package com.tmarzeion.carbuddy.handlers;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/*
* This handler is responsible for:
* - Connecting to Google API
* - Handling connection states
* - Requesting location updates
* - Redirecting updated location events to core application service
*/
public class GoogleApiHandler implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleApiClient;
    private LocationListener onLocationChangedListener;

    public GoogleApiHandler(Context appContext, LocationListener listener) {
        this.googleApiClient = buildGoogleApiClient(appContext);
        this.googleApiClient.connect();
        this.onLocationChangedListener = listener;
    }

    private GoogleApiClient buildGoogleApiClient(Context appContext) {
        return new GoogleApiClient.Builder(appContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    // Connected to Google API
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Don't panic
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Don't panic
    }

    private void requestLocationUpdates() {
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            onLocationChangedListener.onLocationChanged(location);
        }

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(3000); // 3 seconds
        locationRequest.setFastestInterval(1000); // 1 seconds
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, onLocationChangedListener);
    }

}
